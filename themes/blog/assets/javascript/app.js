/*
* Application
*/

require('./bootstrap');


/*
* Document ready
*/
jQuery(document).ready(function($){

    /*
    * Init Categories carousels
    */
    let items = $('#total-categories').val();

    for (var i = 1; i < items; i++) {

        if($('#carousel-categories-'+i+'').length){

            $('#carousel-categories-'+i+'').on('slide.bs.carousel', function (e) {
                var $e = $(e.relatedTarget);
                var idx = $e.index();
                var itemsPerSlide = 5;
                var totalItems = $('.carousel-item-categories').length;

                if (idx >= totalItems-(itemsPerSlide-1)) {

                    var it = itemsPerSlide - (totalItems - idx);

                    for (var i=0; i<it; i++) {

                        // append slides to end
                        if (e.direction=="left") {
                            $('.carousel-item-categories').eq(i).appendTo('.carousel-inner');
                        }
                        else {
                            $('.carousel-item-categories').eq(0).appendTo('.carousel-inner');
                        }
                    }
                }
            });
        }
    }


    if($('#map').length > 0){
        
        // Init Leaflet Map
        // initialize the map on the "map" div with a given center and zoom
        var map = L.map('map', {
            center: [51.505, -0.09],   
            zoom: 13
        });

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoiYWxiZXJ0b21jIiwiYSI6ImNrOG93bzllOTA1YmszZW85dzk5OGdzZTcifQ.IKNEQY7rsLS6I3S3N5nTsQ'
        }).addTo(map);

        let icon = L.divIcon({
            iconUrl: 'https://raw.githubusercontent.com/Leaflet/Leaflet/master/dist/images/marker-icon.png',
            shadowUrl: 'https://raw.githubusercontent.com/Leaflet/Leaflet/master/dist/images/marker-shadow.png'
        });

        // Add marker to map
        var marker = L.marker([51.5, -0.09], { icon: icon }).addTo(map);
        marker.bindPopup("<b>Bloggilife!</b><br>I am a popup.").openPopup();
    }

});
