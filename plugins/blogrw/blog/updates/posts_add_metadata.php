<?php namespace RainLab\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use BlogRW\Blog\Models\Category as CategoryModel;

class PostsAddMetadata extends Migration
{

    public function up()
    {
        if (Schema::hasColumn('blog_posts', 'metadata')) {
            return;
        }

        Schema::table('blog_posts', function($table)
        {
            $table->mediumText('metadata')->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasColumn('blog_posts', 'metadata')) {
            Schema::table('blog_posts', function ($table) {
                $table->dropColumn('metadata');
            });
        }
    }

}
