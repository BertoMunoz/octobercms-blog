<?php namespace BlogRW\Blog\Components;

use Lang;
use Redirect;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use BlogRW\Blog\Models\Post as BlogPost;
use BlogRW\Blog\Models\Category as BlogCategory;
use BlogRW\Blog\Models\Settings as BlogSettings;

class Posts extends ComponentBase
{
    /**
     * A collection of posts to display
     *
     * @var Collection
     */
    public $posts;

    /**
     * Parameter to use for the page number
     *
     * @var string
     */
    public $pageParam;

    /**
     * If the post list should be filtered by a category, the model to use
     *
     * @var Model
     */
    public $category;

    /**
     * Message to display when there are no messages
     *
     * @var string
     */
    public $noPostsMessage;

    /**
     * Message to display in read more button
     *
     * @var string
     */
    public $readMore;

    /**
     * Message to display in see all posts button
     *
     * @var string
     */
    public $seeAllPosts;

    /**
     * Reference to the page name for linking to posts
     *
     * @var string
     */
    public $postPage;

    /**
     * Reference to the page name for linking to posts
     *
     * @var string
     */
    public $postsPage;

    /**
     * Reference to the page name for linking to categories
     *
     * @var string
     */
    public $categoryPage;
    

    public function componentDetails()
    {
        return [
            'name'        => 'blogrw.blog::lang.settings.posts_title',
            'description' => 'blogrw.blog::lang.settings.posts_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'pageNumber' => [
                'title'       => 'blogrw.blog::lang.settings.posts_pagination',
                'description' => 'blogrw.blog::lang.settings.posts_pagination_description',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'categoryFilter' => [
                'title'       => 'blogrw.blog::lang.settings.posts_filter',
                'description' => 'blogrw.blog::lang.settings.posts_filter_description',
                'type'        => 'string',
                'default'     => '',
            ],
            'postsPerPage' => [
                'title'             => 'blogrw.blog::lang.settings.posts_per_page',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'blogrw.blog::lang.settings.posts_per_page_validation',
                'default'           => '5',
            ],
            'noPostsMessage' => [
                'title'             => 'blogrw.blog::lang.settings.posts_no_posts',
                'description'       => 'blogrw.blog::lang.settings.posts_no_posts_description',
                'type'              => 'string',
                'default'           => Lang::get('blogrw.blog::lang.settings.posts_no_posts_default'),
                'showExternalParam' => false,
            ],
            'readMore' => [
                'title'             => 'blogrw.blog::lang.settings.read_more_message',
                'description'       => 'blogrw.blog::lang.settings.read_more_message_description',
                'type'              => 'string',
                'default'           => Lang::get('blogrw.blog::lang.settings.read_more'),
                'showExternalParam' => false,
            ],
            'seeAllPosts' => [
                'title'             => 'blogrw.blog::lang.settings.see_all_posts_message',
                'description'       => 'blogrw.blog::lang.settings.see_all_posts_message_description',
                'type'              => 'string',
                'default'           => Lang::get('blogrw.blog::lang.settings.see_all_posts'),
                'showExternalParam' => false,
            ],
            'categoryPage' => [
                'title'       => 'blogrw.blog::lang.settings.posts_category',
                'description' => 'blogrw.blog::lang.settings.posts_category_description',
                'type'        => 'dropdown',
                'default'     => 'categoria',
                'group'       => 'blogrw.blog::lang.settings.group_links',
            ],
            'postPage' => [
                'title'       => 'blogrw.blog::lang.settings.posts_post',
                'description' => 'blogrw.blog::lang.settings.posts_post_description',
                'type'        => 'dropdown',
                'default'     => 'entrada',
                'group'       => 'blogrw.blog::lang.settings.group_links',
            ],
            'postsPage' => [
                'title'       => 'blogrw.blog::lang.settings.posts_page_title',
                'description' => 'blogrw.blog::lang.settings.posts_page_description',
                'type'        => 'dropdown',
                'default'     => Lang::get('blogrw.blog::lang.settings.posts_page'),
                'group'       => 'blogrw.blog::lang.settings.group_links',
            ]
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getPostPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->prepareVars();

        $this->category = $this->page['category'] = $this->loadCategory();
        $this->posts = $this->page['posts'] = $this->listPosts();

        /*
         * If the page number is not valid, redirect
         */
        if ($pageNumberParam = $this->paramName('pageNumber')) {
            $currentPage = $this->property('pageNumber');

            if ($currentPage > ($lastPage = $this->posts->lastPage()) && $currentPage > 1) {
                return Redirect::to($this->currentPageUrl([$pageNumberParam => $lastPage]));
            }
        }
    }

    protected function prepareVars()
    {
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->noPostsMessage = $this->page['noPostsMessage'] = $this->property('noPostsMessage');
        $this->readMore = $this->page['readMore'] = $this->property('readMore');
        $this->seeAllPosts = $this->page['seeAllPosts'] = $this->property('seeAllPosts');

        /*
         * Page links
         */
        $this->postsPage = $this->page['postsPage'] = $this->property('postsPage');
        $this->postPage = $this->page['postPage'] = $this->property('postPage');
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
    }

    protected function listPosts()
    {
        $category = $this->category ? $this->category->id : null;

        /*
         * List all the posts, eager load their categories
         */
        $posts = BlogPost::with('categories')->where('is_published', true)->paginate(5);

        /*
         * Add a "url" helper attribute for linking to each post and category
         */
        $posts->each(function($post) {
            $post->setUrl($this->postPage, $this->controller);

            $post->categories->each(function($category) {
                $category->setUrl($this->categoryPage, $this->controller);
            });
        });

        return $posts;
    }

    protected function loadCategory()
    {
        if (!$slug = $this->property('categoryFilter')) {
            return null;
        }

        $category = new BlogCategory;

        $category = $category->isClassExtendedWith('BlogRW.Translate.Behaviors.TranslatableModel')
            ? $category->transWhere('slug', $slug)
            : $category->where('slug', $slug);

        $category = $category->first();

        return $category ?: null;
    }
}
