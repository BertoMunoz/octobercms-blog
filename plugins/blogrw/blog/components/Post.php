<?php namespace BlogRW\Blog\Components;

Use Url;
Use Lang;
use Event;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use BlogRW\Blog\Models\Post as BlogPost;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Post extends ComponentBase
{
    /**
     * @var BlogRW\Blog\Models\Post The post model used for display.
     */
    public $post;

    /**
     * @var string Reference to the page name for linking to categories.
     */
    public $categoryPage;

    /**
     * @var string Text inside the return back button.
     */
    public $returnBackMessage;

    public function componentDetails()
    {
        return [
            'name'        => 'blogrw.blog::lang.settings.post_title',
            'description' => 'blogrw.blog::lang.settings.post_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'blogrw.blog::lang.settings.post_slug',
                'description' => 'blogrw.blog::lang.settings.post_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
            'categoryPage' => [
                'title'       => 'blogrw.blog::lang.settings.post_category',
                'description' => 'blogrw.blog::lang.settings.post_category_description',
                'type'        => 'dropdown',
                'default'     => 'categoria',
            ],
            'returnBackMessage' => [
                'title'             => 'blogrw.blog::lang.settings.return_back_message',
                'description'       => 'blogrw.blog::lang.settings.return_back_message_description',
                'type'              => 'string',
                'default'           => Lang::get('blogrw.blog::lang.settings.return_back'),
                'showExternalParam' => false,
            ]
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function init()
    {
        Event::listen('translate.localePicker.translateParams', function ($page, $params, $oldLocale, $newLocale) {
            $newParams = $params;

            foreach ($params as $paramName => $paramValue) {
                $records = BlogPost::transWhere($paramName, $paramValue, $oldLocale)->first();

                if ($records) {
                    $records->translateContext($newLocale);
                    $newParams[$paramName] = $records[$paramName];
                }
            }
            return $newParams;
        });
    }

    public function onRun()
    {
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
        $this->post = $this->page['post'] = $this->loadPost();
        $this->returnBackMessage = $this->page['returnBackMessage'] = $this->property('returnBackMessage');
        $this->page["return_url"] = Url::previous();
    }

    public function onRender()
    {
        if (empty($this->post)) {
            $this->post = $this->page['post'] = $this->loadPost();
        }
    }

    protected function loadPost()
    {
        $slug = $this->property('slug');

        $post = new BlogPost;

        $post = $post->isClassExtendedWith('RainLab.Translate.Behaviors.TranslatableModel')
            ? $post->transWhere('slug', $slug)
            : $post->where('slug', $slug);

        try {

            $post = $post->firstOrFail();

        } catch (ModelNotFoundException $ex) {

            $this->setStatusCode(404);
            return $this->controller->run('404');

        }

        /*
         * Add a "url" helper attribute for linking to each category
         */
        if ($post && $post->categories->count()) {
            $post->categories->each(function($category) {
                $category->setUrl($this->categoryPage, $this->controller);
            });
        }

        return $post;
    }
}
