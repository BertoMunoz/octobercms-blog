<?php namespace BlogRW\Blog\Components;

Use Url;
use Event;
use Cms\Classes\ComponentBase;
use BlogRW\Blog\Models\Category as BlogCategory;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Category extends ComponentBase
{
    /**
     * @var BlogRW\Blog\Models\Category The category model used for display.
     */
    public $category;

    /**
     * @var string Reference to the page name for linking to posts.
     */
    public $postPage;


    public function componentDetails()
    {
        return [
            'name'        => 'Category item Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'blogrw.blog::lang.settings.category_slug',
                'description' => 'blogrw.blog::lang.settings.category_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
            'postPage' => [
                'title'       => 'blogrw.blog::lang.settings.post_category',
                'description' => 'blogrw.blog::lang.settings.post_category_description',
                'type'        => 'dropdown',
                'default'     => 'categoria',
            ]
        ];
    }

    public function onRun()
    {
        $this->postPage = $this->page['postPage'] = $this->property('postPage');
        $this->category = $this->page['category'] = $this->loadCategory();
        $this->page["return_url"] = Url::previous();
    }

    public function onRender()
    {
        if (empty($this->category)) {
            $this->category = $this->page['category'] = $this->loadCategory();
        }
    }

    public function init()
    {
        Event::listen('translate.localePicker.translateParams', function ($page, $params, $oldLocale, $newLocale) {
            $newParams = $params;

            foreach ($params as $paramName => $paramValue) {
                $records = BlogCategory::transWhere($paramName, $paramValue, $oldLocale)->first();

                if ($records) {
                    $records->translateContext($newLocale);
                    $newParams[$paramName] = $records[$paramName];
                }
            }
            return $newParams;
        });
    }

    /**
     * Load Category Model
     * @return mixed
     */
    protected function loadCategory()
    {
        $slug = $this->property('slug');

        $category = new BlogCategory;

        $category = $category->isClassExtendedWith('RainLab.Translate.Behaviors.TranslatableModel')
            ? $category->transWhere('slug', $slug)
            : $category->where('slug', $slug);

        try {

            $category = $category->with(['posts', 'featured_images'])->first();

        } catch (ModelNotFoundException $ex) {

            $this->setStatusCode(404);
            return $this->controller->run('404');

        }

        /*
        * Add a "url" helper attribute for linking posts
        */
        if ($category && $category->posts->count()) {
            $category->posts->each(function($post) {
                $post->setUrl($this->postPage, $this->controller);
            });
        }

        return $category;
    }
}
