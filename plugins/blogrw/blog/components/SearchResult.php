<?php namespace BlogRW\Blog\Components;

use Input;
use Redirect;
use Lang;
use App;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use BlogRW\Blog\Models\Category as BlogCategory;
use BlogRW\Blog\Models\Post as BlogPost;

/**
 * Search Result component
 *
 * @see BlogRW\Blog\Components\Posts
 * @package BlogRW\Blog\Components
 */
class SearchResult extends ComponentBase
{
    /**
     * Parameter to use for the search
     * @var string
     */
    public $searchParam;

    /**
     * The search term
     * @var string
     */
    public $searchTerm;

    /**
     * A collection of posts to display
     * @var Collection
     */
    public $posts;

    /**
     * Parameter to use for the page number
     * @var string
     */
    public $pageParam;

    /**
     * Reference to the page name for linking to posts.
     * @var string
     */
    public $postPage;

    /**
     * Reference to the page name for linking to categories.
     * @var string
     */
    public $categoryPage;

    /**
     * Message to display when there are no messages
     *
     * @var string
     */
    public $noPostsMessage;

    /**
     * Message to display in read more button
     *
     * @var string
     */
    public $readMore;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Search Result',
            'description' => 'Displays a list of blog posts that match the search term on the page.'
        ];
    }

    /**
     * @see BlogRW\Blog\Components\Posts::defineProperties()
     * @return array
     */
    public function defineProperties()
    {

        $categoryItems = BlogCategory::lists('name', 'id');

        return [
            'searchTerm' => [
                'title'       => 'Search Term',
                'description' => 'The value to determine what the user is searching for.',
                'type'        => 'string',
                'default'     => '{{ :search }}',
            ],
            'pageNumber' => [
                'title'       => 'blogrw.blog::lang.settings.posts_pagination',
                'description' => 'blogrw.blog::lang.settings.posts_pagination_description',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'hightlight' => [
                'title'       => 'Hightlight Matches',
                'type'        => 'checkbox',
                'default'     => true,
                'showExternalParam' => false
            ],
            'postsPerPage' => [
                'title'             => 'blogrw.blog::lang.settings.posts_per_page',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'blogrw.blog::lang.settings.posts_per_page_validation',
                'default'           => '10',
            ],
            'sortOrder' => [
                'title'       => 'blogrw.blog::lang.settings.posts_order',
                'description' => 'blogrw.blog::lang.settings.posts_order_description',
                'type'        => 'dropdown',
                'default'     => 'published_at desc'
            ],
            'categoryPage' => [
                'title'       => 'blogrw.blog::lang.settings.posts_category',
                'description' => 'blogrw.blog::lang.settings.posts_category_description',
                'type'        => 'dropdown',
                'default'     => 'categoria',
                'group'       => 'Links',
            ],
            'postPage' => [
                'title'       => 'blogrw.blog::lang.settings.posts_post',
                'description' => 'blogrw.blog::lang.settings.posts_post_description',
                'type'        => 'dropdown',
                'default'     => 'entrada',
                'group'       => 'Links',
            ],
            'disableUrlMapping' => [
                'title'       => 'Disable URL Mapping',
                'description' => 'If the url Mapping is disabled the search form uses the default GET Parameter q '
                . '(e.g. example.com/search?search=Foo instead of example.com/search/Foo)',
                'type'        => 'checkbox',
                'default'     => false,
                'showExternalParam' => false
            ],
            'noPostsMessage' => [
                'title'             => 'blogrw.blog::lang.settings.posts_no_posts',
                'description'       => 'blogrw.blog::lang.settings.posts_no_posts_description',
                'type'              => 'string',
                'default'           => Lang::get('blogrw.blog::lang.settings.posts_no_posts_default'),
                'showExternalParam' => false,
            ],
            'readMore' => [
                'title'             => 'blogrw.blog::lang.settings.read_more_message',
                'description'       => 'blogrw.blog::lang.settings.read_more_message_description',
                'type'              => 'string',
                'default'           => Lang::get('blogrw.blog::lang.settings.read_more'),
                'showExternalParam' => false,
            ]
        ];
    }

    /**
     * @see BlogRW\Blog\Components\Posts::onRun()
     * @return mixed
     */
    public function onRun()
    {
        $this->prepareVars();

        // map get request to :search param
        $searchTerm = Input::get('search');
        $categoryId = Input::get('category');

        if (!$this->property('disableUrlMapping') && \Request::isMethod('get') && $searchTerm) {

            $cat = Input::get('category');
            $query = http_build_query(['cat' => $cat]);
            $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
            $query = !empty($query) ? '?' . $query : '';

            return Redirect::to(
                $this->currentPageUrl([$this->searchParam => urlencode($searchTerm)]). $query
            );
        }

        // load posts
        $this->posts = $this->page['posts'] = $this->listPosts($categoryId);
    }

    /**
     * @see BlogRW\Blog\Components\Posts::prepareVars()
     */
    protected function prepareVars()
    {
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->searchParam = $this->page['searchParam'] = $this->paramName('searchTerm');
        $this->searchTerm = $this->page['searchTerm'] = urldecode($this->property('searchTerm'));

        $this->noPostsMessage = $this->page['noPostsMessage'] = $this->property('noPostsMessage');
        $this->readMore = $this->page['readMore'] = $this->property('readMore');

        /*
         * Page links
         */
        $this->postPage = $this->page['postPage'] = $this->property('postPage');
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
    }

    /**
     * List Posts
     *
     * @return array
     */
    protected function listPosts($categoryId)
    {
        // Filter posts
        $locale = App::getLocale();

        if($locale == 'es'){

            $posts = BlogPost::where(function ($q) {
                $q->where('title', 'LIKE', "%{$this->searchTerm}%")
                ->orWhere('content', 'LIKE', "%{$this->searchTerm}%")
                ->orWhere('summary', 'LIKE', "%{$this->searchTerm}%");
            })->get();

        } else {

            $postsTitles = BlogPost::transWhere('title', "%{$this->searchTerm}%", $locale, 'LIKE')->get();
            $postsContents = BlogPost::transWhere('content', "%{$this->searchTerm}%", $locale, 'LIKE')->get();
            $postsSummaries = BlogPost::transWhere('summary', "%{$this->searchTerm}%", $locale, 'LIKE')->get();

            $posts = $postsTitles->merge($postsContents);
            $posts = $posts->merge($postsSummaries);
        }

        $catId = Input::get('category');
        $posts->whereHas('categories', function(Builder $query) use ($catId) {
            $q->whereIn('id', $catId);
        });

        /*
         * Add a "url" helper attribute for linking to each post and category
         */
        $posts->each(function ($post) {

            $post->setUrl($this->postPage, $this->controller);

            $post->categories->each(function ($category) {
                $category->setUrl($this->categoryPage, $this->controller);
            });

            // apply highlight of search result
            $this->highlight($post);
        });

        return $posts->paginate(5);
    }

    /**
     * @param \BlogRW\Blog\Models\Post $post
     */
    protected function highlight(BlogPost $post)
    {
        if ($this->property('hightlight')) {
            $searchTerm = preg_quote($this->searchTerm, '|');

            // apply highlight
            $post->title = preg_replace('|(' . $searchTerm . ')|iu', '<mark>$1</mark>', $post->title);
            $post->summary = preg_replace('|(' . $searchTerm . ')|iu', '<mark>$1</mark>', $post->summary);

            $post->content_html = preg_replace(
                '~(?![^<>]*>)(' . $searchTerm . ')~ismu',
                '<mark>$1</mark>',
                $post->content_html
            );
        }
    }
}
