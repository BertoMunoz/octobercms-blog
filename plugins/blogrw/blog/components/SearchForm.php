<?php namespace BlogRW\Blog\Components;

use Lang;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use BlogRW\Blog\Models\Category as BlogCategory;

/**
* Search Form Component
*
* @package BlogRW\BlogSearch\Components
*/
class SearchForm extends ComponentBase
{
    /**
     * @var string
     */
    public $resultPage;

    /**
     * @var Collection A collection of categories to display
     */
    public $categories;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Search Form',
            'description' => 'Outputs a search form for the blog.'
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'resultPage' => [
                'title'   => 'Search Results Page',
                'type'    => 'dropdown',
                'default' => Lang::get('blogrw.blog::lang.settings.result_page_link')
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getResultPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Prepare vars
     */
    public function onRun()
    {
        $this->resultPage = $this->page['resultPage'] = $this->property('resultPage');
        $this->categories = $this->page['categories'] = $this->loadCategories();
    }

    /**
     * Load all categories
     *
     * @return array
     */
    protected function loadCategories()
    {
        $categories = BlogCategory::where('is_published', true)->get();

        return $categories;
    }
}
