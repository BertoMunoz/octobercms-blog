<?php namespace BlogRW\Blog\Components;

use Lang;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use BlogRW\Blog\Models\Category as BlogCategory;
use BlogRW\Blog\Models\Post as BlogPost;

class Categories extends ComponentBase
{
    /**
     * @var Collection A collection of categories to display
     */
    public $categories;

    /**
     * @var Collection A collection of posts to display
     */
    public $posts;

    /**
     * Parameter to use for the page number
     *
     * @var string
     */
    public $pageParam;

    /**
     * @var string Reference to the page name for linking to categories.
     */
    public $categoryPage;

    /**
     * @var string Reference to the current category slug.
     */
    public $currentCategorySlug;

    /**
     * Message to display when there are no messages
     *
     * @var string
     */
    public $noCategoriesMessage;

    /**
     * Message to display in read more button
     *
     * @var string
     */
    public $readMore;


    public function componentDetails()
    {
        return [
            'name'        => 'CategoriesPosts',
            'description' => 'Categories with last 5 posts'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'blogrw.blog::lang.settings.category_slug',
                'description' => 'blogrw.blog::lang.settings.category_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
            'displayEmpty' => [
                'title'       => 'blogrw.blog::lang.settings.category_display_empty',
                'description' => 'blogrw.blog::lang.settings.category_display_empty_description',
                'type'        => 'checkbox',
                'default'     => 0,
            ],
            'categoryPage' => [
                'title'       => 'blogrw.blog::lang.settings.category_page',
                'description' => 'blogrw.blog::lang.settings.category_page_description',
                'type'        => 'dropdown',
                'default'     => 'categoria',
                'group'       => 'blogrw.blog::lang.settings.group_links',
            ],
            'noCategoriesMessage' => [
                'title'             => 'blogrw.blog::lang.settings.posts_no_categories',
                'description'       => 'blogrw.blog::lang.settings.posts_no_categories_description',
                'type'              => 'string',
                'default'           => Lang::get('blogrw.blog::lang.categories.no_categories'),
                'showExternalParam' => false,
            ],
            'readMore' => [
                'title'             => 'blogrw.blog::lang.settings.read_more_message',
                'description'       => 'blogrw.blog::lang.settings.read_more_message_description',
                'type'              => 'string',
                'default'           => Lang::get('blogrw.blog::lang.settings.read_more'),
                'showExternalParam' => false,
            ]
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->currentCategorySlug = $this->page['currentCategorySlug'] = $this->property('slug');
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
        $this->categories = $this->page['categories'] = $this->loadCategories();
        $this->readMore = $this->page['readMore'] = $this->property('readMore');
        $this->noCategoriesMessage = $this->page['noCategoriesMessage'] = $this->property('noCategoriesMessage');
    }

    /**
     * Load all categories
     *
     * @return mixed
     */
    protected function loadCategories()
    {
        $categories = BlogCategory::with(['posts.featured_images', 'featured_images', 'posts_count'])
        ->where('is_published', true)->paginate(5);

        /*
         * Add a "url" helper attribute for linking to each category
         */
        return $this->linkCategories($categories);
    }

    /**
     * Sets the URL on each category according to the defined category page
     * @return void
     */
    protected function linkCategories($categories)
    {
        return $categories->each(function ($category) {
            $category->setUrl($this->categoryPage, $this->controller);
        });
    }

}
