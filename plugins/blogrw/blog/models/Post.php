<?php namespace BlogRW\Blog\Models;

use App;
use Model;
use Markdown;
use Carbon\Carbon;
use Cms\Classes\Page as CmsPage;
use BlogRW\Blog\Classes\TagProcessor;

/**
 * Post Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blog_posts';

    /**
    * @var model use RainLab.translate plugin
    */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'slug',
        'summary',
        'content',
        'content_html',
        'published_at',
        'is_published'
    ];

    /**
     * @var array Translatable fields
     */
    public $translatable = [
        'title',
        'content',
        'content_html',
        'summary',
        'metadata',
        ['slug', 'index' => true]
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        'title'   => 'required',
        'slug'    => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:blog_posts'],
        'content' => 'required',
        'summary' => ''
    ];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [
        'metadata'
    ];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [
        'id'
    ];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'published_at'
    ];

    /**
     * The attributes on which the post list can be ordered.
     * @var array
     */
    public static $allowedSortingOptions = [
        'title asc'         => 'blogrw.blog::lang.sorting.title_asc',
        'title desc'        => 'blogrw.blog::lang.sorting.title_desc',
        'published_at asc'  => 'blogrw.blog::lang.sorting.published_asc',
        'published_at desc' => 'blogrw.blog::lang.sorting.published_desc'
    ];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'categories' => [
            'BlogRw\Blog\Models\Category',
            'table' => 'blog_posts_categories',
            'order' => 'name'
        ]
    ];
    public $attachMany = [
        'featured_images' => ['System\Models\File', 'order' => 'sort_order'],
        'content_images'  => ['System\Models\File']
    ];

    public $preview = null;


    //
    // Scopes
    //
    public function scopeIsPublished($query)
    {
        return $query
            ->whereNotNull('is_published')
            ->where('is_published', true)
            ->whereNotNull('published_at')
            ->where('published_at', '<', Carbon::now())
        ;
    }

    /**
    * Before save, store content in html format
    */
    public function beforeSave()
    {
        $this->content_html = self::formatHtml($this->content);
    }

    /**
    * After validate, store published date
    */
    public function afterValidate()
    {
        if ($this->is_published) {
            $this->published_at = Carbon::now();
        } else {
            $this->published_at = NULL;
        }
    }

    /**
    *  Parse Markdown content
    * @param    $input
    * @param    $preview
    * @return   $array
    */
    public static function formatHtml($input, $preview = false)
    {
        $result = Markdown::parse(trim($input));

        if ($preview) {
            $result = str_replace('<pre>', '<pre class="prettyprint">', $result);
        }

        $result = TagProcessor::instance()->processTags($result, $preview);

        return $result;
    }
    

    /**
     * Sets the "url" attribute with a URL to this object.
     * @param string $pageName
     * @param Controller $controller
     *
     * @return string
     */
    public function setUrl($pageName, $controller)
    {
        $locale = App::getLocale();

        switch ($locale) {
            case 'es':
                $pageName = 'entrada';
                break;
            case 'en':
                $pageName = 'post';
                break;
            case 'fr':
                $pageName = 'poste';
                break;
            default:
                $pageName = 'entrada';
                break;
        }

        return $this->url = $pageName.'/'.$this->slug;
    }
}
