<?php namespace BlogRW\Blog\Models;

use Str;
use Model;
use App;
use BlogRW\Blog\Models\Post;
use October\Rain\Router\Helper as RouterHelper;
use Cms\Classes\Page as CmsPage;

/**
 * Category Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var model use RainLab.translate plugin
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blog_categories';

    /**
     * @var array Attributes that support translation, if available.
     */
    public $translatable = [
        'name',
        'description',
        'metadata',
        ['slug', 'index' => true]
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        'name' => 'required',
        'slug' => 'required|between:3,64|unique:blog_categories',
        'code' => 'nullable|unique:blog_categories',
    ];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [
        'metadata'
    ];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'posts' => ['BlogRW\Blog\Models\Post','table' => 'blog_posts_categories','order' => 'published_at desc'],
        'posts_count' => ['BlogRW\Blog\Models\Post', 'table' => 'blog_posts_categories', 'scope' => 'isPublished', 'count' => true]
    ];
    public $attachMany = [
        'featured_images' => ['System\Models\File', 'order' => 'sort_order'],
        'content_images'  => ['System\Models\File']
    ];

    public $preview = null;

    //
    // Scopes
    //
    public function scopeIsCategoryPublished($query)
    {
        return $query
            ->whereNotNull('is_published')
            ->where('is_published', true);
    }


    /**
    * Before validate run this method
    */
    public function beforeValidate()
    {
        // Generate a URL slug for this model
        if (!$this->exists && !$this->slug) {
            $this->slug = Str::slug($this->name);
        }
    }

    /**
    * After delete run this method
    */
    public function afterDelete()
    {
        $this->posts()->detach();
    }

    /**
    * Get post count attribute
    *
    * @return int
    */
    public function getPostCountAttribute()
    {
        return optional($this->posts_count->first())->count ?? 0;
    }

    /**
     * Sets the "url" attribute with a URL to this object
     *
     * @param string $pageName
     * @param Cms\Classes\Controller $controller
     *
     * @return string
     */
    public function setUrl($pageName, $controller)
    {
        $locale = App::getLocale();

        switch ($locale) {
            case 'es':
                $pageName = 'categoria';
                break;
            case 'en':
                $pageName = 'category';
                break;
            case 'fr':
                $pageName = 'categorie';
                break;
            default:
                $pageName = 'categoria';
                break;
        }

        return $this->url = $pageName.'/'.$this->slug;
    }

}
