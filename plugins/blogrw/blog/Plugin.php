<?php namespace BlogRW\Blog;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;
use BlogRW\Blog\Models\Post;
use BlogRW\Blog\Models\Category;

/**
 * Blog Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Blog',
            'description' => 'Blog para la prueba de becarios de Laravel - October',
            'author'      => 'Refinería Web',
            'icon'        => 'icon-pencil',
            'iconSvg'     => 'plugins/blogrw/blog/assets/images/blog-icon.svg'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'BlogRW\Blog\Components\Post' => 'Post',
            'BlogRW\Blog\Components\Posts' => 'Posts',
            'BlogRW\Blog\Components\Category' => 'Category',
            'BlogRW\Blog\Components\Categories' => 'Categories',
            'BlogRW\Blog\Components\SearchForm'   => 'searchForm',
            'BlogRW\Blog\Components\SearchResult' => 'searchResult'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
            'blogrw.blog.manage_settings' => [
                'tab'   => 'blogrw.blog::lang.blog.tab',
                'label' => 'blogrw.blog::lang.blog.manage_settings'
            ],
            'blogrw.blog.access_posts' => [
                'tab'   => 'blogrw.blog::lang.blog.tab',
                'label' => 'blogrw.blog::lang.blog.access_posts'
            ],
            'blogrw.blog.access_categories' => [
                'tab'   => 'blogrw.blog::lang.blog.tab',
                'label' => 'blogrw.blog::lang.blog.access_categories'
            ],
            'blogrw.blog.access_other_posts' => [
                'tab'   => 'blogrw.blog::lang.blog.tab',
                'label' => 'blogrw.blog::lang.blog.access_other_posts'
            ],
            'blogrw.blog.access_publish' => [
                'tab'   => 'blogrw.blog::lang.blog.tab',
                'label' => 'blogrw.blog::lang.blog.access_publish'
            ]
        ];
    }

    public function registerSettings()
    {
        return [
            'blog' => [
                'label' => 'blogrw.blog::lang.blog.menu_label',
                'description' => 'blogrw.blog::lang.blog.settings_description',
                'category' => 'blogrw.blog::lang.blog.menu_label',
                'icon' => 'icon-pencil',
                'class' => 'BlogRW\Blog\Models\Settings',
                'order' => 500,
                'keywords' => 'blog post category',
                'permissions' => ['blogrw.blog.manage_settings']
            ]
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'blog' => [
                'label'       => 'Blog',
                'url'         => Backend::url('blogrw/blog/posts'),
                'icon'        => 'icon-pencil',
                'iconSvg'     => 'plugins/blogrw/blog/assets/images/blog-icon.svg',
                'permissions' => ['blogrw.blog.*'],
                'order'       => 500,
                'sideMenu' => [
                    'new_post' => [
                        'label'       => 'blogrw.blog::lang.posts.new_post',
                        'icon'        => 'icon-plus',
                        'url'         => Backend::url('blogrw/blog/posts/create'),
                        'permissions' => ['blogrw.blog.access_posts']
                    ],
                    'posts' => [
                        'label'       => 'blogrw.blog::lang.blog.posts',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('blogrw/blog/posts'),
                        'permissions' => ['blogrw.blog.access_posts']
                    ],
                    'categories' => [
                        'label'       => 'blogrw.blog::lang.blog.categories',
                        'icon'        => 'icon-list-ul',
                        'url'         => Backend::url('blogrw/blog/categories'),
                        'permissions' => ['blogrw.blog.access_categories']
                    ]
                ]
            ],
        ];
    }

    /**
     * Register new Twig variables
     * @return array
     */
    public function registerMarkupTags()
    {
        // Check the translate plugin is installed
        if (class_exists('RainLab\Translate\Behaviors\TranslatableModel')) {
            return [];
        }

        return [
            'filters' => [
                '_'  => ['Lang', 'get'],
                '__' => ['Lang', 'choice']
            ]
        ];
    }
}
