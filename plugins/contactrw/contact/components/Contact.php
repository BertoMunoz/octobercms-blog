<?php namespace ContactRW\Contact\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Exception\ValidationException;
use ContactRW\Contact\Models\Contact as ContactItem;

class Contact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'contactrw.contact::lang.form.component_name',
            'description' => 'contactrw.contact::lang.form.component_description'
        ];
    }

    public function onSend()
    {
        // Get request data
        $data = \Input::only([
            'first_name',
            'last_name',
            'phone',
            'email',
            'terms_and_conditions',
            'message'
        ]);

        // Validate request
        $this->validate($data);

        // Send email
        $receiver = '';

        \Mail::send('contactrw.contact::contact', $data, function ($message) use ($receiver) {
            $message->to($receiver);
        });

        ContactItem::create([
           'first_name' => $data['first_name'],
           'last_name' => $data['last_name'],
           'email' => $data['email'],
           'phone' => $data['phone'],
           'message' => $data['message']
        ]);
    }

    protected function validate(array $data) 
    {
        // Validate request
        $rules = [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'email' => 'required|email',
            'message' => 'required',
            'terms_and_conditions' => 'accepted',
            'phone' => '',
        ];

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }
}
