<?php namespace ContactRw\Contact\Models;

use October\Rain\Database\Model;

class Settings extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'contactrw_contact_settings';

    public $settingsFields = 'fields.yaml';

    public $rules = [
        'show_recaptcha' => ['boolean'],
        'recaptcha_site_key' => ['string'],
    ];
}
