<?php

return [
	'plugin' => [
		'name' => 'Contact',
		'description' => 'Plugin simple para poner un formulario de contacto y la gestión de los mensajes.',
		'menu_label' => 'Contact',
		'contacts_list' => 'Contacts list'	
	],
	'form' => [
		'component_name' => 'Contact form',
		'component_name' => 'Simple contact form',
		'full_name' => 'Full name',
		'first_name' => 'First name',
		'last_name' => 'Last name',
		'message' => 'Message',
		'email' => 'Email',
		'send' => 'Send',
		'created_at' => 'Sended at',
		'message_send_success' => 'Message send!',
		'message_send_error' => 'Error when send message'
	],
	'settings' => [
		'title' => 'Form preferences',
		'description' => 'Configurar las preferencias del formulario de contacto.',
		'show' => 'Show contact',
		'show_recaptcha_label' => 'Google Recaptcha',
		'show_recaptcha_comment' => 'Show recaptcha',
		'recaptcha_site_key' => 'Recaptcha stie key',
		'recaptcha_site_key_comment' => 'Clave del sitio proporcionada por google para el Recaptcha',
		'tab_general' => 'General'
	]
];