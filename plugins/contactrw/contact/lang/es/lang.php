<?php

return [
	'plugin' => [
		'name' => 'Contacto',
		'description' => 'Plugin simple para poner un formulario de contacto y la gestión de los mensajes.',
		'menu_label' => 'Contacto',
		'contacts_list' => 'Lista de mensajes'	
	],
	'form' => [
		'component_name' => 'Formulario contacto',
		'component_name' => 'Formulario de contacto simple',
		'full_name' => 'Nombre completo',
		'first_name' => 'Nombre',
		'last_name' => 'Apellidos',
		'message' => 'Mensaje',
		'email' => 'Correo electrónico',
		'send' => 'Enviar',
		'created_at' => 'Fecha enviado',
		'message_send_success' => 'Mensaje enviado con éxito!',
		'message_send_error' => 'Error al enviar el mensaje'
	],
	'settings' => [
		'title' => 'Preferencias del formulario',
		'description' => 'Configurar las preferencias del formulario de contacto.',
		'show' => 'Mostrar contacto',
		'show_recaptcha_label' => 'Recaptcha',
		'show_recaptcha_comment' => 'Mostrar recaptcha',
		'recaptcha_site_key' => 'Clave del sitio para el Recaptcha',
		'recaptcha_site_key_comment' => 'Clave del sitio proporcionada por google para el Recaptcha',
		'tab_general' => 'General'
	]
];