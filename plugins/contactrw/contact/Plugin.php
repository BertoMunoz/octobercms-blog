<?php namespace ContactRW\Contact;

use Backend;
use System\Classes\PluginBase;

/**
 * Contact Form Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'contactrw.contact::lang.plugin.name',
            'description' => 'contactrw.contact::lang.plugin.description',
            'author'      => 'Refinería Web',
            'icon'        => 'icon-envelope-o'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'ContactRW\Contact\Components\Contact' => 'contactForm',
        ];
    }

    /**
     * Registers page snippets used by this plugin.
     *
     * @return array
     */
    public function registerPageSnippets()
    {
        return [
            'ContactRW\Contact\Components\Contact' => 'contactForm',
        ];
    }

    /**
     * Register back-end settings used by this plugin.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'contact' => [
                'label' => 'contactrw.contact::lang.settings.title',
                'description' => 'contactrw.contact::lang.settings.description',
                'category' => 'contactrw.contact::lang.plugin.menu_label',
                'icon' => 'icon-pencil',
                'class' => 'ContactRW\Contact\Models\Settings',
                'order' => 500,
                'keywords' => 'contact',
                'permissions' => ['contactrw.contact.*']
            ]
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'contactrw.contact.some_permission' => [
                'tab' => 'Contact',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'contact' => [
                'label'       => 'contactrw.contact::lang.plugin.name',
                'url'         => Backend::url('contactrw/contact/contacts'),
                'icon'        => 'icon-envelope-o',
                'iconSvg'     => 'plugins/contactrw/contact/assets/img/mail.svg',
                'permissions' => ['contactrw.contact.*'],
                'order'       => 500,
            ],
        ];
    }
}
