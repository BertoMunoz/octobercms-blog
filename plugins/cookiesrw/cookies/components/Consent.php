<?php namespace CookiesRW\Cookies\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use CookiesRW\Cookies\Models\Consent as CookieConsent;
use CookiesRW\Cookies\Models\CookieType as Types;

class Consent extends ComponentBase
{

    /**
     * @var CookiesRW\Cookies\Models\Consent The cookie consent used for display.
     */
    public $consent;

    /**
     * @var CookiesRW\Cookies\Models\CookieType Cookie site types.
     */
    public $cookie_types;

    public function componentDetails()
    {
        return [
            'name'        => 'Consent Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {

        $this->addJs('/plugins/CookiesRW/Cookies/assets/js/jquery.ihavecookies.js');
        $this->addCss('/plugins/CookiesRW/Cookies/assets/css/cookie_consent.css');
        $this->consent = $this->page['consent'] = $this->loadConsent();
        $this->cookie_types = $this->page['cookie_types'] = $this->loadCookiesTypes();
    }

    protected function loadCookiesTypes()
    {
        $cookie_types = new Types;
        return $cookie_types->where('id', '!=', 1)->get();
    }

    protected function loadConsent()
    {
        $consent = new CookieConsent;
        return $consent->first();
    }
}
