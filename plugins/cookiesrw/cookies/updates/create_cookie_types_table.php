<?php namespace CookiesRW\Cookies\Updates;

use Schema;
use DB;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCookieTypesTable extends Migration
{
    public function up()
    {
        Schema::create('cookiesrw_cookie_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('content');
            $table->text('content');
            $table->text('domain');
            $table->text('path');
            $table->text('send_for');
            $table->date('expires');
            $table->timestamps();
        });

        DB::table('cookiesrw_cookie_types')->insert([
            'name' => 'Necesary',
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('cookiesrw_cookie_types');
    }
}
