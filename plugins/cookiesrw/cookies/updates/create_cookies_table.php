<?php namespace CookiesRW\Cookies\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCookiesTable extends Migration
{
    public function up()
    {
        Schema::create('cookiesrw_cookies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('value');
            $table->text('description')->nullable();

            $table->unsignedInteger('type_id');
            $table->foreign('type_id')->references('id')->on('cookiesrw_cookie_types');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cookiesrw_cookies');
    }
}
