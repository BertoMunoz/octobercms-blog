<?php namespace CookiesRW\Cookies\Updates;

use Schema;
use DB;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateConsentsTable extends Migration
{
    public function up()
    {
        Schema::create('cookiesrw_cookies_consents', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->text('consent_size');
            $table->text('consent_float');
            $table->text('consent_position');

            $table->text('title');
            $table->text('title_color');
            $table->longText('message');
            $table->text('message_color');

            $table->text('link');
            $table->text('link_text');
            $table->text('link_color');
            $table->text('link_color_hover');
            
            $table->integer('expires')->default(30)->nullable();

            $table->text('accept_btn_text');
            $table->text('accept_btn_background_color');
            $table->text('accept_btn_text_color');

            $table->text('accept_btn_background_color_hover');
            $table->text('accept_btn_text_color_hover');

            $table->text('advanced_btn_text');
            $table->text('advanced_btn_text_color');
            $table->text('advanced_btn_background_color');

            $table->text('advanced_btn_background_color_hover');
            $table->text('advanced_btn_text_color_hover');

            $table->text('cookie_types_title');
            $table->text('cookie_types_title_color');

            $table->text('fixed_cookie_type_text');
            $table->text('fixed_cookie_type_text_color');

            $table->text('cookie_types_description');
            $table->text('cookie_types_description_color');       

            $table->boolean('uncheck_boxes');   
            $table->text('background_color');      

            $table->timestamps();
        });

        DB::table('cookiesrw_cookies_consents')->insert([

            'consent_size' => 'mini',
            'consent_float' => 'right',
            'consent_position' => 'bottom',

            'title' => 'Cookies & Privacy',
            'title_color' => '#FFFFFF',

            'message' => 'Cookies enable you to use shopping carts and to personalize your experience on our sites, tell us which parts of our websites people have visited, help us measure the effectiveness of ads and web searches, and give us insights into user behavior so we can improve our communications and products.',
            'message_color' => '#FFFFFF',

            'link' => '/privacy-policy',
            'link_text' => 'More information',
            'link_color' => '#039199',
            'link_color_hover' => '#037EAD', 

            'accept_btn_text' => 'Accept Cookies',
            'accept_btn_background_color' => '#039199',
            'accept_btn_text_color' => '#FFFFFF',
            'accept_btn_background_color_hover' => '#037EAD',
            'accept_btn_text_color_hover' => '#666666',

            'advanced_btn_text' => 'Customise Cookies',
            'advanced_btn_text_color' => '#FFFFFF',
            'advanced_btn_background_color' => '#037EAD',
            'advanced_btn_background_color_hover' => '#039199',
            'advanced_btn_text_color_hover' => '#666666',

            'cookie_types_title' => 'Select cookies to accept',
            'cookie_types_title_color' => '#FFFFFF',

            'fixed_cookie_type_text' => 'Necessary',
            'fixed_cookie_type_text_color' => '#FFFFFF',

            'cookie_types_description' => 'These are cookies that are essential for the website to work correctly.',
            'cookie_types_description_color' => '#FFFFFF',

            'expires' => 30, 
            'uncheck_boxes' => false,
            'background_color' => '#037EAD',
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('cookiesrw_cookies_consents');
    }
}
