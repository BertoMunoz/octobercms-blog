<?php namespace CookiesRW\Cookies\Controllers;

use BackendMenu;
use Cms\Classes\Theme as CmsTheme;
use Backend\Classes\Controller;

/**
 * Consents Back-end Controller
 */
class Consents extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController'
    ];

    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->addCss('/plugins/cookiesrw/cookies/assets/css/cookie_admin.css');
        $this->addJs('/plugins/cookiesrw/cookies/assets/js/attrchange.js');

        BackendMenu::setContext('CookiesRW.Cookies', 'cookies', 'consents');

        $code = CmsTheme::getActiveThemeCode();
        $theme = CmsTheme::load($code);
        $preview = $theme->getPreviewImageUrl();
        $this->vars['preview']=$preview;
    }
}
