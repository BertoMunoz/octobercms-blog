<?php namespace CookiesRW\Cookies;

use Backend;
use System\Classes\PluginBase;

/**
 * Cookies Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'cookiesrw.cookies::lang.plugin.name',
            'description' => 'cookiesrw.cookies::lang.plugin.desription',
            'author'      => 'Refinería Web',
            'icon'        => 'icon-soccer-ball-o'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'CookiesRW\Cookies\Components\Consent' => 'cookie_consent'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'cookiesrw.cookies.some_permission' => [
                'tab' => 'Cookies',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'cookies' => [
                'label'       => 'cookiesrw.cookies::lang.plugin.name',
                'url'         => Backend::url('cookiesrw/cookies/consents/update/1'),
                'icon'        => 'icon-soccer-ball-o',
                'iconSvg'     => 'plugins/cookiesrw/cookies/assets/images/cookie-icon.svg',
                'permissions' => ['cookiesrw.cookies.*'],
                'order'       => 500,
                'sideMenu' => [
                    'consent' => [
                        'label'       => 'cookiesrw.cookies::lang.consent.menu_label',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('cookiesrw/cookies/consents/update/1'),
                        'permissions' => ['cookiesrw.cookies.*']
                    ],
                    'cookies' => [
                        'label'       => 'cookiesrw.cookies::lang.cookie.menu_label',
                        'icon'        => 'icon-list-ul',
                        'url'         => Backend::url('cookiesrw/cookies/cookies'),
                        'permissions' => ['cookiesrw.cookies.*']
                    ],
                    'types' => [
                        'label'       => 'cookiesrw.cookies::lang.cookieType.menu_label',
                        'icon'        => 'icon-list-ul',
                        'url'         => Backend::url('cookiesrw/cookies/cookietypes'),
                        'permissions' => ['cookiesrw.cookies.*']
                    ]
                ]
            ]
        ];
    }
}
