<?php

return [
	'plugin' => [
        'name' => 'Cookies',
        'description' => 'A GDPR Cookies message.',

        's' => 'G',
        'ave' => 'uardar',
        'or' => 'o',
        'saving_consent' => 'Guardando aviso de cookies',
        'return_back' => 'Volver atrás',
        'create' => 'Crear',
        'create_and_close' => 'Crear y salir',
        'cancel' => 'Cancelar',
    ],
    'consent' => [
        'menu_label' => 'Aviso de Cookies',
        'edit_consent' => 'Editar aviso de cookies',
        'preview_consent' => 'Previsualizar aviso de cookies',

        'consent_size' => 'Tamaño del aviso',
        'consent_float' => 'Dirección del aviso',
        'consent_position' => 'Posición del aviso',

        'right'=>'Derecha',
        'left'=>'Izquierda',
        'top'=>'Arriba',
        'bottom'=>'Abajo',

        'mini'=> 'Pequeño',
        'large'=> 'Grande',

        'title' => 'Título',
        'title_placeholder' => 'Escribe un título para el aviso',
        'title_color' => 'Color del título',

        'message' => 'Mensaje',
        'message_placeholder' => 'Escribe un mensaje para el aviso de cookies...',
        'message_color' => 'Color del mensaje',

        'link' => 'Enlace',
        'link_text' => 'Texto para el enlace',
        'link_text_color' => 'Color del enlace',
        'link_color_hover' => 'Color enlace secundario',

        'expires' => 'Tiempo expiración cookies', 

        'accept_btn_text_color' => 'Color del texto del botón - Aceptar -',
        'accept_btn_background_color' => 'Color del fondo del botón - Aceptar -',
        'accept_btn_text' => 'Texto del botón - Aceptar -',

        'advanced_btn_text_color' => 'Color texto botón avanzado',
        'advanced_btn_background_color' => 'Color fondo botón avanzado',
        'advanced_btn_background_color_hover' => 'Color fondo secundario botón avanzado',
        'advanced_btn_text_color_hover' => 'Color texto secundario botón avanzado',
        'advanced_btn_text' => 'Texto botón avanzado',

        'cookie_types_title_color' => 'Color título tipos de cookies',
        'cookie_types_title' => 'Texto título tipos de cookies',

        'fixed_cookie_type_color' => 'Color del texto de tipos de cookies',
        'fixed_cookie_type_text' => 'Texto tipos de cookies',

        'cookie_types_description_color' => 'Color descripcion de los tipos de cookies',
        'cookie_types_description' => 'Descripción tipos cookies',

        'uncheck_boxes' => 'Desbloquear páginas al iniciar'
    ],
    'cookie' => [
        'menu_label' => 'Cookies',
        'manage' => 'Manage Cookies',

        'create' => 'Añadir cookie',
        'creating' => 'Creando cookie...',

        'delete_selected' => 'Eliminar seleccionada',
        'deleting' => 'Eliminando cookie...',
        'delete_question' => 'Delete this cookie?',
        'delete_confirm' => 'Estas seguro que quieres eliminar las cookies seleccionadas?',

        'update' => 'Modificar cookie',
        'saving' => 'Guardando cookie...',

        'preview' => 'Previsualizar cookie',

        'name' => 'Nombre',
        'description' => 'Descripción',

        'return_to_list' => 'Volver a la lista de cookies'
    ],
    'cookieType' => [
        'menu_label' => 'Tipos de Cookies',
        'name' => 'Nombre',
        'description' => 'Descripción',
        'manage_cookie_types' => 'Manage Cookie Types',

        'creating' => 'Creating CookieType...',
        'create' => 'Añadir nuevo tipo',
        'delete_selected' => 'Eliminar seleccionado',
        'deleting' => 'Eliminando tipo...',
        'delete_question' => 'Delete this cookie type?',
        'delete_confirm' => 'Estas seguro que quieres eliminar los tipos cookies seleccionados?',
        'update' => 'Modificar tipo',
        'saving' => 'Guardando tipo...',
        'preview' => 'Previsualizar tipo cookie',

        'return_to_cookie_types_list' => 'Volver a la lista de cookies'
    ]
];