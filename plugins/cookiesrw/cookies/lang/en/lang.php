<?php

return [
    'plugin' => [
        'name' => 'Cookies',
        'description' => 'A GDPR Cookies message.',

        's' => 'S',
        'ave' => 'ave',
        'or' => 'or',
        'saving_consent' => 'Saving cookie notice',
        'return_back' => 'Return Back',
        'create' => 'Create',
        'create_and_close' => 'Create and exit',
        'cancel' => 'Cancel'
    ],
    'consent' => [
        'menu_label' => 'Cookie Notice',
        'edit_consent' => 'Edit cookie notice',
        'preview_consent' => 'Preview cookie notice',

        'title' => 'Title',
        'title_placeholder' => 'Write a title...',
        'title_color' => 'Title color',

        'message' => 'Message',
        'message_placeholder' => 'Write a message for the cookie notice ...',
        'message_color' => 'Message text color',

        'link' => 'Privacy policy page link',
        'link_text_color' => 'Link text color',
        'link_color_hover' => 'Link text hover color',

        'expires' => 'Cookies expiration time',

        'more_info_text_color' => 'Text color for - More information -',
        'more_info_text' => 'Text for - More information -',

        'accept_btn_text_color' => 'Text color for - Accept - button',
        'accept_btn_background_color' => 'Background color button - Accept -',
        'accept_btn_text' => 'Text button - Accept -',

        'advanced_btn_text_color' => 'Text color advanced button',
        'advanced_btn_background_color' => 'Background color advanced button',
        'advanced_btn_background_color_hover' => 'Hover background color advanceed button',
        'advanced_btn_text_color_hover' => 'Hover text color advanced button',
        'advanced_btn_text' => 'Advanced button text',

        'cookie_types_title_color' => 'Color title cookie types',
        'cookie_types_title' => 'Cookie types title',

        'fixed_cookie_type_color' => 'Fixed cookie type text color',
        'fixed_cookie_type_text' => 'Fixed cookie types text',

        'cookie_types_description_color' => 'Cookye types description color',
        'cookie_types_description' => 'Cookie types description',

        'uncheck_boxes' => 'Uncheck checkboxes on load'
    ],
    'cookie' => [
        'menu_label' => 'Cookies',
        'manage' => 'Manage Cookies',

        'create' => 'Add cookie',
        'creating' => 'Creating cookie...',

        'delete_selected' => 'Delete selected',
        'deleting' => 'Deleting cookie...',
        'delete_question' => 'Delete this cookie?',
        'delete_confirm' => 'Are you sure you want to delete the selected cookies?',

        'update' => 'Update cookie',
        'saving' => 'Saving cookie...',

        'preview' => 'Preview cookie',

        'name' => 'Name',
        'description' => 'Description',

        'return_to_list' => 'Return back to cookies list'
    ],
    'cookieType' => [
        'menu_label' => 'Cookie Types',
        'name' => 'Name',
        'description' => 'Description',
        'manage_cookie_types' => 'Manage Cookie Types',

        'creating' => 'Creating Cookie Type...',
        'create' => 'Add new type',
        'delete_selected' => 'Delete selected',
        'deleting' => 'Deleting type...',
        'delete_question' => 'Delete this cookie type?',
        'delete_confirm' => 'Are you sure you want to delete the selected cookie types?',
        'update' => 'Update type',
        'saving' => 'Saving type...',
        'preview' => 'Preview cookie type',

        'return_to_cookie_types_list' => 'Return to cookie types list'
    ]
];