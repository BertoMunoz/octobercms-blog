<?php namespace CookiesRW\Cookies\Models;

use Model;

/**
 * CookieType Model
 */
class CookieType extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
    * @var model use RainLab.translate plugin
    */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cookiesrw_cookie_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id',
        'name',
        'description'
    ];

    /**
     * @var array Translatable fields
     */
    public $translatable = [
        'name',
        'description'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        ['name' => 'required']
    ];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = ['id'];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
    * @var array Relations
    */
    public $belongsTo = [];
    public $belongsToMany = [
        'cookies' => [
            'CokiesRW\Cookies\Models\Cookies',
            'table' => 'cookiesrw_cookies',
            'order' => 'value'
        ]
    ];
}
