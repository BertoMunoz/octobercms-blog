<?php namespace CookiesRW\Cookies\Models;

use Model;

/**
 * Cookie Model
 */
class Cookie extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cookiesrw_cookies';

    /**
    * @var model use RainLab.translate plugin
    */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'description',
        'type_id'
    ];

    /**
     * @var array Translatable fields
     */
    public $translatable = [
        'name',
        'description'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        ['name' => 'required']
    ];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [
        'id'
    ];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'type' => [
            'CokiesRW\Cookies\Models\CookieType',
            'table' => 'cookiesrw_cookie_types',
            'order' => 'name'
        ]
    ];
}
