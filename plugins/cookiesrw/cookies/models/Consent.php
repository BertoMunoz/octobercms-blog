<?php namespace CookiesRW\Cookies\Models;

use Model;
use ScssPhp\ScssPhp\Compiler;

/**
 * Consent Model
 */
class Consent extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cookiesrw_cookies_consents';

    /**
    * @var model use RainLab.translate plugin
    */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Translatable fields
     */
    public $translatable = [
        'title',
        'message',
        'link',
        'link_text',
        'accept_btn_text',
        'advanced_btn_text',
        'cookie_types_title',
        'fixed_cookie_type_text',
        'cookie_types_title'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [
        'id'
    ];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
    * Before save, set styles from sass to css
    */
    public function beforeSave()
    {

        $scss = new Compiler();

        $scssIn = file_get_contents('./plugins/cookiesrw/cookies/assets/sass/consent.scss');

        $scss->setVariables(array(
            'consent_size' => $this->consent_size,
            'consent_float' => $this->consent_float,
            'consent_position' => $this->consent_position,
            'background_color' => $this->background_color,
            'title_color' => $this->title_color,
            'message_color' => $this->message_color,
            'link_color' => $this->link_color,
            'link_color_hover' => $this->link_color_hover, 
            'accept_btn_background_color' => $this->accept_btn_background_color,
            'accept_btn_text_color' => $this->accept_btn_text_color,
            'accept_btn_background_color_hover' => $this->accept_btn_text_color_hover,
            'accept_btn_text_color_hover' => $this->accept_btn_text_color_hover,
            'advanced_btn_text_color' => $this->advanced_btn_text_color,
            'advanced_btn_background_color' => $this->advanced_btn_background_color,
            'advanced_btn_background_color_hover' => $this->advanced_btn_background_color_hover,
            'advanced_btn_text_color_hover' => $this->advanced_btn_text_color_hover,
            'cookie_types_title_color' => $this->cookie_types_title_color,
            'fixed_cookie_type_text_color' => $this->fixed_cookie_type_text_color,
            'cookie_types_description_color' => $this->cookie_types_description_color
        ));

        $cssOut = $scss->compile($scssIn);
        file_put_contents('./plugins/cookiesrw/cookies/assets/css/cookie_consent.css', $cssOut);
    }
}
