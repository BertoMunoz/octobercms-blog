/*!
* jQuery plugin for displaying cookie/privacy message
*
*/

$(document).ready(function() {

    if (getCookie('cookieconsent_status') === 'validate') {
        $('.cookie-wrapper').css('display', 'none');
    }

    // Uncheck any checkboxes on page load
    if ($('#uncheckBoxes').val() === true) {
        $('input[type="checkbox"].ihavecookies').prop('checked', false);
    }

    // When accept button is clicked drop cookie
    $('#gdpr-cookie-accept').click(function(){

        let expires = $('#expires').val();

        // Set cookie
        dropCookie(true, expires);

        // If 'data-auto' is set to ON, tick all checkboxes because
        // the user hasn't clicked the customise cookies button
        $('input[name="gdpr[]"][data-auto="on"]').prop('checked', true);

        // Save users cookie preferences (in a cookie!)
        var prefs = [];
        $.each($('input[name="gdpr[]"]').serializeArray(), function(i, field){
            prefs.push(field.value);
        });

        setCookie('cookieControlPrefs', encodeURIComponent(JSON.stringify(prefs)), 365);

        // Run callback function
        onAccept();
    });
    
    // Toggle advanced cookie options
    $('#gdpr-cookie-advanced').click(function(){

        // Uncheck all checkboxes except for the disabled 'necessary'
        // one and set 'data-auto' to OFF for all. The user can now
        // select the cookies they want to accept.
        $('input[name="gdpr[]"]:not(:disabled)').attr('data-auto', 'off').prop('checked', false);

        $('#gdpr-cookie-types').slideDown('fast', function(){
            $('#gdpr-cookie-advanced').prop('disabled', true);
        });

    });
});

function onAccept() {
    document.cookie = "cookieconsent_status=validate";
    let myPreferences = cookie();
    console.log('Yay! The following preferences were saved...');
    console.log(myPreferences);
}

// Method to get cookie value
function cookie() {
    var preferences = getCookie('cookieControlPrefs');
    return JSON.parse(preferences);
};

// Method to check if user cookie preference exists
function preference(cookieTypeValue) {
    var control = getCookie('cookieControl');
    var preferences = getCookie('cookieControlPrefs');
    preferences = JSON.parse(preferences);
    if (control === false) {
        return false;
    }
    if (preferences === false || preferences.indexOf(cookieTypeValue) === -1) {
        return false;
    }
    return true;
};

/*
|--------------------------------------------------------------------------
| Drop Cookie
|--------------------------------------------------------------------------
|
| Function to drop the cookie with a boolean value of true.
|
*/
function dropCookie(value, expiryDays) {

    setCookie('cookieControl', value, expiryDays);

    $('#gdpr-cookie-message').fadeOut('fast', function() {
        $(this).remove();
    });
};

/*
|--------------------------------------------------------------------------
| Set Cookie
|--------------------------------------------------------------------------
|
| Sets cookie with 'name' and value of 'value' for 'expiry_days'.
|
*/
function setCookie(name, value, expiry_days) {
    var d = new Date();
    d.setTime(d.getTime() + (expiry_days*24*60*60*1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
    return getCookie(name);
};

/*
|--------------------------------------------------------------------------
| Get Cookie
|--------------------------------------------------------------------------
|
| Gets cookie called 'name'.
|
*/
function getCookie(name) {
    var cookie_name = name + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(cookie_name) === 0) {
            return c.substring(cookie_name.length, c.length);
        }
    }
    return false;
};
